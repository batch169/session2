function factorial(n) {
  if (typeof n !== "number" || n < 0) return undefined;
  if (n === 0 || n === 1) return 1;
  return n * factorial(n - 1);
}

function oddEven(n) {
  if (typeof n !== "number") return undefined;

  if (n % 2 === 0) {
    return "even";
  } else {
    return "odd";
  }
}

function div_check(n) {
  if (typeof n !== "number") return undefined;

  if (n % 5 === 0 || n % 7 === 0) {
    return true;
  } else {
    return false;
  }
}

const names = {
  Name1: {
    name: "Boba Fett",
    age: 50,
  },
  Name2: {
    name: "Anakin Skywalker",
    age: 30,
  },
};

module.exports = {
  factorial: factorial,
  oddEven: oddEven,
  div_check: div_check,
  names,
};
