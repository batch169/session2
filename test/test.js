const { assert, expect } = require("chai");
const { factorial, oddEven, div_check } = require("./../src/util");

describe("Factorial Testing", () => {
  it("Test if factorial 5! is 120", () => {
    const product = factorial(5);
    expect(product).to.equal(120);
  });

  it("Test if factorial 1! is 1", () => {
    const product = factorial(1);
    assert.equal(product, 1);
  });

  it("Test if factorial 0! is 1", () => {
    const product = factorial(0);
    expect(product).to.equal(1);
  });

  it("Test if factorial 4! is 24", () => {
    const product = factorial(4);
    assert.equal(product, 24);
  });

  it("Test if factorial 10! is 3628800", () => {
    const product = factorial(10);
    expect(product).to.equal(3628800);
  });

  it("Test number is not negative", () => {
    let number = factorial(-1);
    expect(number).to.equal(undefined);
  });

  it("Test if value is not a number", () => {
    let value = factorial(1);
    expect(typeof value).to.equal("number");
  });
});

describe("Odd / Even Testing", () => {
  it("Test result is even", () => {
    const result = oddEven(2);
    expect(result).to.equal("even");
  });

  it("Test result is odd", () => {
    const result = oddEven(5);
    assert.equal(result, "odd");
  });

  it("Assert that result is a number", () => {
    const result = oddEven(1);
    assert.notEqual(typeof result, "undefined");
  });
});

describe("5 or 7 Testing", () => {
  it("Test if 105 is divisible by 5", () => {
    const result = div_check(105);
    assert.equal(result, true);
  });

  it("Test if 14 is divisible by 7", () => {
    const result = div_check(14);
    expect(result).to.equal(true);
  });

  it("Test if 0 is divisible by 5 or 7", () => {
    const result = div_check(0);
    assert.equal(result, true);
  });

  it("Test if 22 is divisible by 5 or 7", () => {
    const result = div_check(22);
    expect(result).to.equal(false);
  });

  it("Assert that result is a number", () => {
    const result = div_check(2);
    expect(typeof result).to.not.equal("undefined");
  });
});
