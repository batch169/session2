const chai = require("chai");
const expect = chai.expect;
const http = require("chai-http");
chai.use(http);

describe("Chai HTTP API Testing", () => {
  it("Test API GET People is running", () => {
    chai
      .request("http://localhost:3001")
      .get("/people")
      .end((err, res) => {
        expect(res).to.not.equal(undefined);
      });
  });

  it("Test API GET People returns 200", (done) => {
    chai
      .request("http://localhost:3001")
      .get("/people")
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });

  it("Test API POST Person returns 400 if not found", (done) => {
    chai
      .request("http://localhost:3001")
      .post("/person")
      .type("json")
      .send({
        alias: "Jason",
        age: 15,
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("Check if Person endpoint is running", () => {
    let data = {
      name: "test",
      age: 25,
    };
    chai
      .request("http://localhost:3001")
      .post("/person")
      .send(data)
      .end((err, res) => {
        expect(res.status).to.equal(200);
      });
  });

  it("Test API post person returns 400 if no NAME", (done) => {
    chai
      .request("http://localhost:3001")
      .post("/person")
      .type("json")
      .send({
        age: 25,
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("Test API post person returns 400 if no AGE", (done) => {
    chai
      .request("http://localhost:3001")
      .post("/person")
      .type("json")
      .send({
        name: "Boba Fett",
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });
});
