const { names } = require("./../src/util");

module.exports = (app) => {
  app.get("/", (req, res) => {
    return res.send({ data: {} });
  });

  app.get("/people", (req, res) => {
    return res.send({
      people: names,
    });
  });

  app.post("/person", (req, res) => {
    if (!req.body.hasOwnProperty("name")) {
      return res.status(400).send({
        error: "Bad Request - missing required parameter of NAME",
      });
    }

    if (typeof req.body.name !== "string") {
      return res.status(400).send({
        error: "Bad Request - NAME must be a string",
      });
    }

    if (!req.body.hasOwnProperty("age")) {
      return res.status(400).send({
        error: "Bad Request - missing required parameter of AGE",
      });
    }

    if (typeof req.body.age !== "number") {
      return res.status(400).send({
        error: "Bad Request - AGE must be a number",
      });
    }
  });
};
